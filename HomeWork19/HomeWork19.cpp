﻿// HomeWork19.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//
#include <iostream>
#include <string>
class animal
{
public:
    animal() {}
    virtual void Voice()
    {
        std::cout << "hello animals";
    }
};
class dog :public animal
{
public:
    dog() {}
    virtual void Voice() override { std::cout << "woof"; }
};
class cat :public animal
{
public:
    cat() {}
    virtual void Voice() override { std::cout << "meow"; }
};
class mouse :public animal
{
public:
    mouse() {}
    void Voice() override { std::cout << "py-py-py"; }
};
int main()
{
    dog a;
    cat c;
    mouse m;
    const int x = 3;
    animal arr[x]{ a,c,m };
    for (auto element : arr)
    {
        arr->Voice();
        std::cout << '\n';
    }


}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.

